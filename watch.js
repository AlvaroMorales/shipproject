(function(){
var setHour=function(){
  var date = new Date(),
      time= date.getHours();
      ampm,
      minutos= date.getMinutes(),
      seconds=date.getSeconds(),
      dayweek=date.getDay(),
      day=date.getDate(),
      month=date.getMonth(),
      year=date.GetFullYear();

  var pTime=document.getElementById('time'),
      pampm=document.getElementById('ampm'),
      pMinutes=document.getElementById('minutes'),
      pSeconds=document.getElementById('seconds'),
      pDayWeek=document.getElementById('dayweek'),
      pDay=document.getElementById('day'),
      pMonth=document.getElementById('month'),
      pYear=document.getElementById('year');


  var semana= ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'];
  var meses= ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];

  pDayWeek.textContent= semana[dayweek];
  pMonth.textContent=meses[month];
  pDay.textContent=day;
  pYear.textContent=year;

  if(time>=12){
    time=time-12;
ampm=pm;
  }
  else{
ampm=am;
  }
  if(time==0){
    time=12;
  }
  pTime=time;
  pampm.textContent=ampm;
  if(minutes<10){
    minutes="0"+minutes;
  }
  if(seconds<10){
    seconds="0"+seconds;
  }
  pMinutes.textContent=minutes;
  pSeconds.textContent=seconds;
  document.getElementById('year').innerHtml = year;
};
var setInterval(setHour, 1000);
setHour();
}());
